//
//  MYproduct.h
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYProduct : NSObject
@property (nonatomic) int identifier;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *description;
@property (strong,nonatomic) NSNumber *salePrice;
@property (strong,nonatomic) NSNumber *regularPrice;
@property (strong,nonatomic) UIImage *productPhoto;
@property (strong,nonatomic) NSArray *colors;
@property (strong,nonatomic) NSDictionary *stores;
@end
