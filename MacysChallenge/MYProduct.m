//
//  MYproduct.m
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import "MYProduct.h"

@implementation MYProduct
@synthesize identifier;
@synthesize name;
@synthesize description;
@synthesize salePrice;
@synthesize regularPrice;
@synthesize productPhoto;
@synthesize colors;
@synthesize stores;
@end
