//
//  MYViewController.m
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/26/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import "MYViewController.h"
#import "MYProduct.h"
#import "MYProductDBUtil.h"
#import "MYNewProductViewController.h"

@interface MYViewController ()
@property (strong,nonatomic) UITableView *tableView;
@property (strong,nonatomic) MYProductDBUtil *dbUtil;
@property (strong,nonatomic) MYnewProductViewController *productViewController;
@property (strong,nonatomic) NSMutableArray *productList;
@end

@implementation MYViewController
#define MYBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#pragma mark - View Lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.dbUtil=[MYProductDBUtil sharedDBUtil];
    [self.navigationItem setTitle:@"Product List"];
    
    //create the table view with a given style
    
    CGRect tableFrame=CGRectMake(0,30, self.view.bounds.size.width, self.view.bounds.size.height-30);
    self.tableView = [[UITableView alloc] initWithFrame:tableFrame
                                                    style:UITableViewStyleGrouped];
    
    //set the table view delegate to the current so we can listen for events
    self.tableView.delegate = self;
    //set the datasource for the table view to the current object
    self.tableView.dataSource = self;
    
    //add the table view to the main view
    [self.view addSubview:self.tableView];
    
    //create a submit button in the navigation bar
    UIBarButtonItem *buttonAdd = [[UIBarButtonItem alloc]
                                 initWithTitle:@"Add"
                                 style:UIBarButtonItemStylePlain
                                 target:self
                                 action:@selector(addProduct:)];
    [self.navigationItem setRightBarButtonItem:buttonAdd];
    
    UIBarButtonItem *buttonFetch = [[UIBarButtonItem alloc]
                                 initWithTitle:@"fetch"
                                 style:UIBarButtonItemStylePlain
                                 target:self
                                 action:@selector(fetchProducts:)];
    [self.navigationItem setLeftBarButtonItem:buttonFetch];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //get Employee List
    self.productList = [self.dbUtil getProducts];
    //reload the data in the table view
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.productList count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    MYProduct *selectedProduct=[self.productList objectAtIndex:indexPath.row];
    cell.textLabel.text=selectedProduct.name;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MYProduct *selectedProduct=[self.productList objectAtIndex:indexPath.row];
    [self showProduct:selectedProduct];

}

#pragma mark - Private Methods

- (void) addProduct:(id)sender {
    
    //get reference to the button that requested the action
    UIBarButtonItem *myButton = (UIBarButtonItem *)sender;
    
    //check which button it is, if you have more than one button on the screen
    //you must check before taking necessary action
    if([myButton.title isEqualToString:@"Add"]){
        NSLog(@"Clicked on add product button");
        MYnewProductViewController *newProdView = [[MYnewProductViewController alloc] init];
            self.productViewController = newProdView;
       //tell the navigation controller to push a new view into the stack
        [self.navigationController pushViewController:self.productViewController animated:YES];
        
    }
    
}

-(void)fetchProducts:(id)sender{

    [self LoadAndParseJson];
}

-(void)LoadAndParseJson{
    NSDictionary *jsonData=[self dictionaryWithContentsOfJSON];
    if (jsonData!=nil) {
        //Get the products
        NSArray *products=[jsonData objectForKey:@"products"];
        for (id product in products) {
            //For each product get the attributes
            MYProduct *newProduct=[[MYProduct alloc] init];
            newProduct.name=[product objectForKey:@"name"];
            NSString *imageUrl=[product objectForKey:@"product photo"];
            NSLog(@"\nUrl:%@",imageUrl);
            newProduct.description=[product objectForKey:@"description"];
            NSString *regularPrice=[product objectForKey:@"regular price"];
            newProduct.regularPrice=[NSNumber numberWithDouble:[regularPrice doubleValue]];
            NSString *salePrice=[product objectForKey:@"sale price"];
            newProduct.salePrice=[NSNumber numberWithDouble:[salePrice doubleValue]];
            id colors=[product objectForKey:@"colors"];
            NSMutableArray *colorsArray=[[NSMutableArray alloc] initWithObjects: nil];
            for (id color in colors) {
                [colorsArray addObject:color];
            }
            newProduct.colors=colorsArray;
            NSMutableDictionary *stores=[product objectForKey:@"stores"];
            newProduct.stores=stores;
            //Fetch the image on a background thread and then add the object to database
            dispatch_async(MYBgQueue, ^{
                NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                if (imgData) {
                    UIImage *image = [UIImage imageWithData:imgData];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            newProduct.productPhoto=image;
                            [self.dbUtil saveProduct:newProduct];
                            self.productList=[self.dbUtil getProducts];
                            [self.tableView reloadData];
                        });
                    }
                }
            });

            
        }
    }
}

-(NSDictionary*)dictionaryWithContentsOfJSON{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"productsJson" ofType:@"txt"];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    if (error != nil) {
        NSLog(@"error fetching Json %@",error);
        return nil;
    };
    return result;
}


-(void)showProduct:(MYProduct *)product{

        MYnewProductViewController *newProdView = [[MYnewProductViewController alloc] init];
        self.productViewController = newProdView;
    self.productViewController.presentingProduct=product;
    //tell the navigation controller to push a new view into the stack
    [self.navigationController pushViewController:self.productViewController animated:YES];

}
@end
