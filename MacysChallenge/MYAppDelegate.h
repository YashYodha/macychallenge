//
//  MYAppDelegate.h
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/26/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
