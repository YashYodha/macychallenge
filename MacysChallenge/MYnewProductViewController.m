//
//  MYnewProductViewController.m
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import "MYnewProductViewController.h"
#import "MYProductDBUtil.h"
#import "MYproduct.h"

@interface MYnewProductViewController(){
    UIImagePickerController *imagePicker;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProduct;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDescription;
@property (weak, nonatomic) IBOutlet UITextField *textfieldSalesPrice;
@property (weak, nonatomic) IBOutlet UITextField *textFieldRegularPrice;
@property (weak, nonatomic) IBOutlet UITextField *textFieldColors;
@property (weak, nonatomic) IBOutlet UITextView *textViewStores;
@property (weak, nonatomic) IBOutlet UIButton *buttonAdd;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (strong,nonatomic) MYProductDBUtil *dbUtil;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)addProduct:(id)sender;
- (IBAction)addImage:(id)sender;
- (IBAction)editProduct:(id)sender;

@end

@implementation MYnewProductViewController
#define kOFFSET_FOR_KEYBOARD 120.0
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dbUtil=[MYProductDBUtil sharedDBUtil];
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height*1.5)];
    [self.textFieldName setDelegate:self];
    [self.textFieldDescription setDelegate:self];
    [self.textfieldSalesPrice setDelegate:self];
    [self.textFieldRegularPrice setDelegate:self];
    [self.textFieldColors setDelegate:self];
    [self.textViewStores setDelegate:self];
    [self.textfieldSalesPrice setKeyboardType:UIKeyboardTypeNumberPad];
    [self.textFieldRegularPrice setKeyboardType:UIKeyboardTypeNumberPad];
    [self.textFieldColors setKeyboardType:UIKeyboardTypeAlphabet];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    self.textFieldRegularPrice.inputAccessoryView = numberToolbar;
    self.textfieldSalesPrice.inputAccessoryView = numberToolbar;
    [self.buttonEdit setHidden:YES];
    
    if (self.presentingProduct!=nil) {
        [self setUpProduct:self.presentingProduct];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IB Methods

- (IBAction)addProduct:(id)sender {
    
    if ([self.textFieldDescription.text isEqualToString:@""] || (self.textFieldDescription.text==nil) || [self.textFieldName.text isEqualToString:@""] || (self.textFieldName.text==nil) || [self.textFieldRegularPrice.text isEqualToString:@""] || (self.textFieldRegularPrice.text==nil) || [self.textfieldSalesPrice.text isEqualToString:@""] || (self.textfieldSalesPrice.text==nil) || [self.textViewStores.text isEqualToString:@""] || (self.textViewStores.text==nil) || [self.textFieldColors.text isEqualToString:@""] || (self.textFieldColors.text==nil)) {
        
        UIAlertView *alertViewemptyField=[[UIAlertView alloc] initWithTitle:@"Fields Cannot be empty" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertViewemptyField show];
        
    }else{
    //Create and add a new product
        MYProduct *newProduct=[[MYProduct alloc] init];
        newProduct.name=self.textFieldName.text;
        newProduct.description=self.textFieldDescription.text;
        newProduct.salePrice=[NSNumber numberWithDouble:[self.textfieldSalesPrice.text doubleValue]];
        newProduct.regularPrice=[NSNumber numberWithDouble:[self.textFieldRegularPrice.text doubleValue]];
        newProduct.colors=[self.textFieldColors.text componentsSeparatedByString:@","];
        newProduct.productPhoto=self.imageViewProduct.image;
        if ([self.dbUtil saveProduct:newProduct]) {
            UIAlertView *successFullAdditionOfitem = [[UIAlertView alloc] initWithTitle:@"Item added succesully" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [successFullAdditionOfitem show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (IBAction)addImage:(id)sender {
    imagePicker=[[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
    imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
    }else{
    imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePicker.delegate=self;
    imagePicker.allowsEditing=TRUE;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)editProduct:(id)sender {
    
    if ([self.textFieldDescription.text isEqualToString:@""] || (self.textFieldDescription.text==nil) || [self.textFieldName.text isEqualToString:@""] || (self.textFieldName.text==nil) || [self.textFieldRegularPrice.text isEqualToString:@""] || (self.textFieldRegularPrice.text==nil) || [self.textfieldSalesPrice.text isEqualToString:@""] || (self.textfieldSalesPrice.text==nil) || [self.textViewStores.text isEqualToString:@""] || (self.textViewStores.text==nil) || [self.textFieldColors.text isEqualToString:@""] || (self.textFieldColors.text==nil)) {
        
        UIAlertView *alertViewemptyField=[[UIAlertView alloc] initWithTitle:@"Fields Cannot be empty" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertViewemptyField show];
        
    }else{
        //Create and add a new product
        self.presentingProduct.name=self.textFieldName.text;
        self.presentingProduct.description=self.textFieldDescription.text;
        self.presentingProduct.salePrice=[NSNumber numberWithDouble:[self.textfieldSalesPrice.text doubleValue]];
        self.presentingProduct.regularPrice=[NSNumber numberWithDouble:[self.textFieldRegularPrice.text doubleValue]];
        self.presentingProduct.colors=[self.textFieldColors.text componentsSeparatedByString:@","];
        self.presentingProduct.productPhoto=self.imageViewProduct.image;
        NSData *jsondata = [self.textViewStores.text dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsondata options:0 error:&error];
        self.presentingProduct.stores=jsonDict;
        if ([self.dbUtil saveProduct:self.presentingProduct]) {
            UIAlertView *successfullUpdationOfitem = [[UIAlertView alloc] initWithTitle:@"Item Updated succesully" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [successfullUpdationOfitem show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }

    
}

#pragma mark - Textfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{

}

#pragma mark - private methods
-(void)setUpProduct:(MYProduct *)product{
    NSLog(@"Presenting %@",product.name);
    self.presentingProduct=product;
    self.textFieldName.text=product.name;
    self.textFieldDescription.text=product.description;
    self.textFieldRegularPrice.text=[NSString stringWithFormat:@"%f",[product.regularPrice doubleValue]];
    self.textfieldSalesPrice.text=[NSString stringWithFormat:@"%f",[product.salePrice doubleValue]];
    self.imageViewProduct.image=product.productPhoto;
    NSString *colorstring=@"";
    for (id color in product.colors) {
        colorstring=[colorstring stringByAppendingString:[NSString stringWithFormat:@"%@,",color]];
    }
    self.textFieldColors.text=colorstring;
    self.textViewStores.text=[self convertDictionaryToString:[product.stores copy]];
    [self.buttonAdd setHidden:YES];
    [self.buttonEdit setHidden:NO];
    
    //Add delete functionality
    
    UIBarButtonItem *buttonDelete = [[UIBarButtonItem alloc]
                                     initWithTitle:@"Delete"
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(deleteProduct:)];
    [self.navigationItem setRightBarButtonItem:buttonDelete];

    
}

-(void)doneWithNumberPad{
    [self.textFieldRegularPrice resignFirstResponder];
    [self.textfieldSalesPrice resignFirstResponder];
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO];
    }
}


-(NSString*) convertDictionaryToString:(NSMutableDictionary*) dict
{
    NSError* error;
    NSDictionary* tempDict = [dict copy]; // get Dictionary from mutable Dictionary
    //giving error as it takes dic, array,etc only. not custom object.
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:tempDict
                                                       options:NSJSONReadingMutableLeaves error:&error];
    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
                                             encoding:NSUTF8StringEncoding];
    return nsJson;
}

-(void)deleteProduct:(id)sender{

    if([self.dbUtil deleteProduct:self.presentingProduct]){
        UIAlertView *deletionSuccesfull=[[UIAlertView alloc]initWithTitle:@"Succesfully Deleted" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [deletionSuccesfull show];
    }
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - ImagePickerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
    self.imageViewProduct.image=image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - textview delegates

@end
