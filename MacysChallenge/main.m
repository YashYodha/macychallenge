//
//  main.m
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/26/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MYAppDelegate class]));
    }
}
