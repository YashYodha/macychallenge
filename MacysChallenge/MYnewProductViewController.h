//
//  MYnewProductViewController.h
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYProduct.h"
@interface MYnewProductViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>
@property (nonatomic,strong) MYProduct *presentingProduct;
@end
