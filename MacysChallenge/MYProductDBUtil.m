//
//  MYProductDBUtil.m
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import "MYProductDBUtil.h"
#import <sqlite3.h>
@interface MYProductDBUtil(){
    sqlite3 *mySQLLiteDB;
}
@end

@implementation MYProductDBUtil
@synthesize databasePath;

//Create a singleton
+ (id)sharedDBUtil{
    static MYProductDBUtil *sharedDBUtil = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDBUtil = [[self alloc] init];
    });
    return sharedDBUtil;
}

-(id)init{
    NSLog(@"Initialising New DB");
    self=[super init];
    if (self) {
        [self initDatabase];
    }
    return self;
}

#pragma - mark DataBase Initailisation
- (void) initDatabase {
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent:@"products.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    //the file will not be there when we load the application for the first time
    //so this will create the database table
    if ([filemgr fileExistsAtPath: databasePath ] == NO){
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &mySQLLiteDB) == SQLITE_OK){
            char *errMsg;
            NSString *sql_stmt = @"CREATE TABLE IF NOT EXISTS PRODUCTS (";
            sql_stmt = [sql_stmt stringByAppendingString:@"id INTEGER PRIMARY KEY AUTOINCREMENT, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"name TEXT, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"description TEXT, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"saleprice TEXT, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"regularprice TEXT, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"Image BLOB, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"colors BLOB, "];
            sql_stmt = [sql_stmt stringByAppendingString:@"stores BLOB)"];
            
            if (sqlite3_exec(mySQLLiteDB, [sql_stmt UTF8String], NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table");
            }
            else{
                NSLog(@"Products table created successfully");
            }
            sqlite3_close(mySQLLiteDB);
            
        }
        else{
            NSLog(@"Failed to open/create database");
        }
    }
    
}

//save our data
- (BOOL) saveProduct:(MYProduct *)product
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [databasePath UTF8String];
    //Convert image to data
    NSData *productImage= UIImagePNGRepresentation(product.productPhoto);
    
    //convert array and dictionary to data
    NSData *colordata = [NSKeyedArchiver archivedDataWithRootObject:product.colors];
    
    NSArray *decodedarray=[NSKeyedUnarchiver unarchiveObjectWithData:colordata];
    NSLog(@"%@",decodedarray);
    
    NSData *storedata = [NSKeyedArchiver archivedDataWithRootObject:product.stores];
    
    if (sqlite3_open(dbpath, &mySQLLiteDB) == SQLITE_OK){
        if (product.identifier > 0){
            NSLog(@"Exitsing data, Update Please");
            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE PRODUCTS set name = '%@', description = '%@', saleprice = '%@', regularprice = '%@', Image = ?,colors = ?,stores = ? WHERE id = ?",
                                   product.name,
                                   product.description,
                                   [NSString stringWithFormat:@"%f", [product.salePrice doubleValue]],[NSString stringWithFormat:@"%f", [product.regularPrice doubleValue]]];
            const char *update_stmt = [updateSQL UTF8String];
            sqlite3_prepare_v2(mySQLLiteDB, update_stmt, -1, &statement, NULL);
            sqlite3_bind_blob(statement, 1, [productImage bytes],[productImage length],SQLITE_TRANSIENT);
            sqlite3_bind_blob(statement, 2, [colordata bytes],[colordata length],SQLITE_TRANSIENT);
            sqlite3_bind_blob(statement, 3, [storedata bytes],[storedata length],SQLITE_TRANSIENT);
            sqlite3_bind_int(statement,4,product.identifier);
            if (sqlite3_step(statement) == SQLITE_DONE){
                success = true;
            }
        }
        else{
            NSLog(@"New data, Insert Please");
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT INTO PRODUCTS (name, description,saleprice,regularprice,Image,colors,stores) VALUES (\"%@\", \"%@\", \"%@\",\"%@\",?,?,?);",
                                   product.name,
                                   product.description,
                                   [NSString stringWithFormat:@"%f", [product.salePrice doubleValue]],[NSString stringWithFormat:@"%f", [product.regularPrice doubleValue]]];
            
            const char *insert_stmt = [insertSQL UTF8String];
            if ( sqlite3_prepare_v2(mySQLLiteDB, insert_stmt, -1, &statement, NULL)!=SQLITE_OK){
                NSLog(@"problem preparing insert statement");
            }
            sqlite3_bind_blob(statement, 1, [productImage bytes], [productImage length], NULL);
            sqlite3_bind_blob(statement, 2, [colordata bytes],[colordata length],NULL);
            sqlite3_bind_blob(statement, 3, [storedata bytes],[storedata length],SQLITE_TRANSIENT);
            if (sqlite3_step(statement) == SQLITE_DONE){
                success = true;
                NSLog(@"Insertion done");
            }else{
                NSLog(@"inserting failed");
            }
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(mySQLLiteDB);
        
    }
    
    return success;
}

//Get all Products

- (NSMutableArray *) getProducts
{
    NSMutableArray *productList = [[NSMutableArray alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &mySQLLiteDB) == SQLITE_OK){
        NSString *querySQL = @"SELECT id, name, description, saleprice, regularprice, Image,colors,stores FROM PRODUCTS";
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(mySQLLiteDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW){
                MYProduct *product = [[MYProduct alloc] init];
                product.identifier = sqlite3_column_int(statement, 0);
                product.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                product.description = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                product.salePrice =[NSNumber numberWithDouble: sqlite3_column_double(statement, 3)];
                product.regularPrice =[NSNumber numberWithDouble: sqlite3_column_double(statement, 4)];
                
                //Unarchive and convert the blob data into respective formats
                int imgLength = sqlite3_column_bytes(statement,5 );
                NSData *imgData= [NSData dataWithBytes:sqlite3_column_blob(statement, 5) length:imgLength];
                product.productPhoto=[UIImage imageWithData:imgData];
                
                
                int colorlength = sqlite3_column_bytes(statement,6);
                NSData *colorData= [NSData dataWithBytes:sqlite3_column_blob(statement, 6) length:colorlength];
                product.colors=[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                
                int storelength = sqlite3_column_bytes(statement,7);
                NSData *storeData= [NSData dataWithBytes:sqlite3_column_blob(statement, 7) length:storelength];
                product.stores=[NSKeyedUnarchiver unarchiveObjectWithData:storeData];
                
                [productList addObject:product];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(mySQLLiteDB);
    }
    
    return productList;
}


//Get Product
- (MYProduct *) getProduct:(NSInteger)productID
{
    MYProduct *product = [[MYProduct alloc] init];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &mySQLLiteDB) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT id, name, description, saleprice, regularprice, Image,colors,stores FROM PRODUCTS WHERE id=%d",
                              productID];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(mySQLLiteDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_ROW){
                product.identifier = sqlite3_column_int(statement, 0);
                product.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                product.description = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                product.salePrice =[NSNumber numberWithDouble: sqlite3_column_double(statement, 3)];
                product.regularPrice =[NSNumber numberWithDouble: sqlite3_column_double(statement, 4)];
                int imgLength = sqlite3_column_bytes(statement,5 );
                NSData *imgData= [NSData dataWithBytes:sqlite3_column_blob(statement, 5) length:imgLength];
                product.productPhoto=[UIImage imageWithData:imgData];
                
                int colorlength = sqlite3_column_bytes(statement,6);
                NSData *colorData= [NSData dataWithBytes:sqlite3_column_blob(statement, 6) length:colorlength];
                product.colors=[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                
                int storelength = sqlite3_column_bytes(statement,7);
                NSData *storeData= [NSData dataWithBytes:sqlite3_column_blob(statement, 7) length:storelength];
                product.stores=[NSKeyedUnarchiver unarchiveObjectWithData:storeData];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(mySQLLiteDB);
    }
    
    return product;
}

//Delete a product
- (BOOL) deleteProduct:(MYProduct *)product
{
    BOOL success = false;
    sqlite3_stmt *statement = NULL;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &mySQLLiteDB) == SQLITE_OK){
        if (product.identifier > 0){
            NSLog(@"Exitsing data, Delete Please");
            NSString *deleteSQL = [NSString stringWithFormat:@"DELETE from PRODUCTS WHERE id = ?"];
            
            const char *delete_stmt = [deleteSQL UTF8String];
            sqlite3_prepare_v2(mySQLLiteDB, delete_stmt, -1, &statement, NULL );
            sqlite3_bind_int(statement, 1, product.identifier);
            if (sqlite3_step(statement) == SQLITE_DONE){
                success = true;
            }
        }
        else{
            NSLog(@"New data, Nothing to delete");
            success = true;
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(mySQLLiteDB);
        
    }
    
    return success;
}

@end
