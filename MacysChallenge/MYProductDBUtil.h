//
//  MYProductDBUtil.h
//  MacysChallenge
//
//  Created by Yashashvi Kampalli (Student) on 5/27/14.
//  Copyright (c) 2014 Yash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYProduct.h"
@interface MYProductDBUtil : NSObject
@property (nonatomic,strong) NSString *databasePath;
+ (id)sharedDBUtil;
- (BOOL)saveProduct:(MYProduct *)product;
- (BOOL)deleteProduct:(MYProduct *)product;
- (NSMutableArray *)getProducts;
- (MYProduct *)getProduct:(NSInteger)productID;
@end
